﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using practica3ipc22.fcodigo;

namespace practica3ipc22.forms
{
    public partial class adminForm : Form
    {
        public adminForm()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (textBox1.Text!=""&& textBox2.Text != "" && textBox3.Text != "" && textBox4.Text != "" && textBox5.Text != "" &&
                textBox6.Text != "" && textBox7.Text != "")
            {
                string id = textBox1.Text;
                string nombre = textBox2.Text;
                string cantidad = textBox3.Text;
                string precio = textBox4.Text;
                string genero = textBox5.Text;
                string autor = textBox6.Text;
                string descripcion = textBox7.Text;

                consultas.insertarLibro(id, nombre, Int32.Parse(cantidad), Int32.Parse(precio), genero, autor, descripcion);
                tabla("");
            }
            else
            {
                MessageBox.Show("Campos vacios"); 
            }
            
        }

        public void tabla(String a)
        {
            if (a=="")
            {
                dataGridView1.Rows.Clear();
                MySqlConnection coneccion = conexion.RecibirConexion();
                MySqlCommand comando = new MySqlCommand("SELECT *  FROM libro ;", coneccion);
                //comando.Parameters.AddWithValue("@a",a);

                coneccion.Open();
                MySqlDataReader dato = comando.ExecuteReader();
                while (dato.Read())
                {
                    dataGridView1.Rows.Add(dato["id"].ToString(), dato["nombre"].ToString(), dato["cantidad"].ToString(), dato["precio"].ToString()
                        , dato["genero"].ToString(), dato["autor"].ToString(), dato["descripcion"].ToString());

                }
                coneccion.Close();
            }
            else
            {
                dataGridView1.Rows.Clear();
                MySqlConnection coneccion = conexion.RecibirConexion();
                MySqlCommand comando = new MySqlCommand("SELECT *  FROM libro ;", coneccion);
                //comando.Parameters.AddWithValue("@a", a);

                coneccion.Open();
                MySqlDataReader dato = comando.ExecuteReader();
                while (dato.Read())
                {
                    string[] genero = dato["genero"].ToString().Split(',');
                    string[] autor = dato["autor"].ToString().Split(',');

                    for (int i = 0; i < genero.Length; i++) 
                    {
                        if (a==genero[i])
                        {
                            dataGridView1.Rows.Add(dato["id"].ToString(), dato["nombre"].ToString(), dato["cantidad"].ToString(), dato["precio"].ToString()
                        , dato["genero"].ToString(), dato["autor"].ToString(), dato["descripcion"].ToString());
                        }
                    }
                    for (int j = 0; j < autor.Length; j++)
                    {
                        if (a==autor[j]) 
                        {
                            dataGridView1.Rows.Add(dato["id"].ToString(), dato["nombre"].ToString(), dato["cantidad"].ToString(), dato["precio"].ToString()
                        , dato["genero"].ToString(), dato["autor"].ToString(), dato["descripcion"].ToString());
                        }
                    }

                    

                }
                coneccion.Close();
            }
            


        }

        private void adminForm_Load(object sender, EventArgs e)
        {
            tabla("");
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            textBox1.Text = dataGridView1.Rows[e.RowIndex].Cells[0].Value.ToString();
            textBox2.Text = dataGridView1.Rows[e.RowIndex].Cells[1].Value.ToString();
            textBox3.Text = dataGridView1.Rows[e.RowIndex].Cells[2].Value.ToString();
            textBox4.Text = dataGridView1.Rows[e.RowIndex].Cells[3].Value.ToString();
            textBox5.Text = dataGridView1.Rows[e.RowIndex].Cells[4].Value.ToString();
            textBox6.Text = dataGridView1.Rows[e.RowIndex].Cells[5].Value.ToString();
            textBox7.Text = dataGridView1.Rows[e.RowIndex].Cells[6].Value.ToString();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (textBox1.Text != "" && textBox2.Text != "" && textBox3.Text != "" && textBox4.Text != "" && textBox5.Text != "" &&
                textBox6.Text != "" && textBox7.Text != "")
            {
                string id = textBox1.Text;
                string nombre = textBox2.Text;
                string cantidad = textBox3.Text;
                string precio = textBox4.Text;
                string genero = textBox5.Text;
                string autor = textBox6.Text;
                string descripcion = textBox7.Text;

                consultas.editarLibro(id, nombre, Int32.Parse(cantidad), Int32.Parse(precio), genero, autor, descripcion);
                tabla(""); 
            }
            else
            {
                MessageBox.Show("Campos vacios");
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (textBox1.Text != "")
            {
                string id = textBox1.Text;
                 
                consultas.eliminarLibro(id);
                tabla("");
            }
            else
            {
                MessageBox.Show("Campos vacios");
            }
        }

        private void textBox8_TextChanged(object sender, EventArgs e)
        {
            tabla(textBox8.Text); 
        }
    }
}
