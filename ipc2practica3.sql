﻿CREATE DATABASE ipc2practica3;
USE ipc2practica3;


CREATE TABLE IF NOT EXISTS `libro` (
  `id` varchar(50) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `cantidad` int(11) NOT NULL,
  `precio` int(11) NOT NULL,
  `genero` varchar(100) NOT NULL,
  `autor` varchar(100) NOT NULL,
  `descripcion` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) 

