﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace practica3ipc22.fcodigo
{
    class consultas
    {

        public static void insertarLibro(string id, string nombre, int cantidad, int precio, string genero, string autor, string descripcion)
        {

            MySqlConnection conectar = conexion.RecibirConexion();

            try
            {
                conectar.Open();
                MySqlCommand comando = new MySqlCommand();
                comando.Connection = conectar;

                comando.CommandType = System.Data.CommandType.Text;
                comando.CommandText = "INSERT INTO libro VALUES(@a,@b,@c,@d,@e,@f,@g);";

                comando.Parameters.AddWithValue("@a", id);
                comando.Parameters.AddWithValue("@b", nombre);
                comando.Parameters.AddWithValue("@c", cantidad);
                comando.Parameters.AddWithValue("@d", precio);
                comando.Parameters.AddWithValue("@e", genero);
                comando.Parameters.AddWithValue("@f", autor);
                comando.Parameters.AddWithValue("@g", descripcion);

                try
                {
                    comando.ExecuteNonQuery();
                    conectar.Close();
                }
                catch (Exception) { }

            }
            catch (Exception) { }


        }


        public static void editarLibro(string id, string nombre, int cantidad, int precio, string genero, string autor, string descripcion)
        {

            MySqlConnection conectar = conexion.RecibirConexion();

            try
            {
                conectar.Open();
                MySqlCommand comando = new MySqlCommand();
                comando.Connection = conectar;

                comando.CommandType = System.Data.CommandType.Text;
                comando.CommandText = "UPDATE libro SET id =@a,nombre =@b,cantidad =@c,precio =@d,genero =@e,autor =@f,descripcion=@g WHERE  id =@a;";

                comando.Parameters.AddWithValue("@a", id);
                comando.Parameters.AddWithValue("@b", nombre);
                comando.Parameters.AddWithValue("@c", cantidad);
                comando.Parameters.AddWithValue("@d", precio);
                comando.Parameters.AddWithValue("@e", genero);
                comando.Parameters.AddWithValue("@f", autor);
                comando.Parameters.AddWithValue("@g", descripcion);  


                try
                {
                    comando.ExecuteNonQuery();
                    conectar.Close();
                }
                catch (Exception) { }

            }
            catch (Exception) { }


        }


        public static void eliminarLibro(string id)
        {

            MySqlConnection conectar = conexion.RecibirConexion();

            try
            {
                conectar.Open();
                MySqlCommand comando = new MySqlCommand();
                comando.Connection = conectar;

                comando.CommandType = System.Data.CommandType.Text;
                comando.CommandText = "DELETE FROM libro WHERE  id=@a;";

                comando.Parameters.AddWithValue("@a", id);

                try
                {
                    comando.ExecuteNonQuery();
                    conectar.Close();
                }
                catch (Exception) { }

            }
            catch (Exception) { }

        }

    }
}
